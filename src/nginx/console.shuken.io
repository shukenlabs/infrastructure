server {

	server_name console.shuken.io;

	location / {
		add_header 'Content-Security-Policy' 'upgrade-insecure-requests';

                proxy_pass http://localhost:3000;
                proxy_set_header   Host               $host;
                proxy_set_header   X-Real-IP          $remote_addr;
                proxy_set_header   X-Forwarded-Proto  $scheme;
                proxy_set_header   X-Forwarded-For    $proxy_add_x_forwarded_for;
		proxy_set_header   Authorization $http_authorization;
		proxy_pass_header  Authorization;
		proxy_pass_header  X-Auth-Token;
                proxy_pass_header  Content-Type;
        }

	server_name console-react-dev.shuken.io;

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/console-react-dev.shuken.io/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/console-react-dev.shuken.io/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = console-react-dev.shuken.io) {
        return 301 https://$host$request_uri;
    } # managed by Certbot



	server_name console-react-dev.shuken.io;

	listen 80;
	listen [::]:80;

	server_name console-react-dev.shuken.io;
    return 404; # managed by Certbot


}
