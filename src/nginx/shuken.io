server {

    root /data/www/production/shuken.io/www/public;

    index index.php;

    access_log /var/log/nginx/access_shuken.io.log;
    error_log /var/log/nginx/error_shuken.io.log;

    server_name shuken.io www.shuken.io;

    location / {
	#include cors.conf;

        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.4-fpm.sock;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/shuken.io/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/shuken.io/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = shuken.io) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;

    server_name shuken.io;
    return 404; # managed by Certbot


}
