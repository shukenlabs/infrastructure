server {

	server_name api.shuken.io;

	location / {
		proxy_pass http://localhost:5100;

		#add_header 'Content-Security-Policy' 'upgrade-insecure-requests';
                
		#proxy_pass_header  Content-Type;
		#proxy_pass_header  Authorization;
		#proxy_pass_header  X-Auth-Token;
                #proxy_set_header   Host               $host;
                #proxy_set_header   X-Real-IP          $remote_addr;
                #proxy_set_header   X-Forwarded-Proto  $scheme;
                #proxy_set_header   X-Forwarded-For    $proxy_add_x_forwarded_for;
                #proxy_set_header   Authorization      $http_authorization;
        }



    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/api.shuken.io/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/api.shuken.io/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = api.shuken.io) {
        return 301 https://$host$request_uri;
    } # managed by Certbot



	server_name api.shuken.io;



	listen 80;
	listen [::]:80;
    return 404; # managed by Certbot


}